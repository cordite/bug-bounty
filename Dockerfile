FROM cordite/cordite:v0.3.5

EXPOSE 8080 10002

ENV CORDITE_LEGAL_NAME O=YourNameHere, OU=Cordite Foundation, L=London, C=GB
ENV CORDITE_P2P_ADDRESS serveo.net:10002
ENV CORDITE_DETECT_IP false
ENV CORDITE_COMPATIBILITY_ZONE_URL https://nms-bb.cordite.foundation
ENV CORDITE_DEV_MODE true
ENV CORDITE_CACHE_NODEINFO true

RUN rm cordite-log4j2.xml cordapps/metering*.jar cordapps/cordite-cordapp*.jar
COPY bb-cordapp/build/libs/bb-cordapp-0.1.jar cordapps/bb-cordapp-0.1.jar
COPY bb-cordapp-contracts-states/build/libs/bb-cordapp-contracts-states-0.1.jar cordapps/bb-cordapp-contracts-states-0.1.jar
COPY config/dev/log4j2.xml cordite-log4j2.xml

#VOLUME ["/opt/cordite/config", "/opt/cordite/certificates", "/opt/cordite/tls-certificates", "/opt/cordite/db"]