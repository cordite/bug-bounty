<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Prework/Pre-requisites

There are some assumptions we've made to make it a bit easier to have one repo, as such, it would be great if you could
find the time to make sure:

 * you have created a gitlab id and sent it to [bw@chorum.io](mailto:bw@chorum.io)
 * you agree to [Apache ICLA](https://www.apache.org/licenses/icla.pdf)
 * you have [docker and docker-compose](https://docs.docker.com/install/) on your machine (we're on v2 for mac which has engine 18.09.0 and compose 1.23.1)
 * you have a recent version of the java8 jdk on your machine (i'm using java version "1.8.0_181")
 * we know this works with intellij - everyone in the corda world is using intellij, so might be worth installing the 
 [free community edition](https://www.jetbrains.com/idea/download/) even if you are a die hard eclipse enthusiast (i was, fwiw ;-).   
 
 It would also be worth:
 
 * reading the [docs](https://docs.cordite.foundation) to familiarise yourself with Cordite a bit
 * watching the corda con dao video [here](https://www.youtube.com/watch?v=oo7LFN_D5V4&feature=youtu.be)