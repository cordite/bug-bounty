<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Resources

## Useful links
   * http://www.apache.org/foundation/how-it-works.html#decision-making 
   * https://www.apache.org/foundation/voting.html
   * https://www.apache.org/foundation/glossary.html#LazyConsensus
   * https://www.apache.org/foundation/glossary.html#ConsensusApproval
   * https://www.apache.org/foundation/glossary.html#MajorityApproval 
   * Cordite Cycle Analytics - https://gitlab.com/cordite/cordite/cycle_analytics
   * Gitlab Timekeeping https://docs.gitlab.com/ee/workflow/time_tracking.html 
   * Gitlab API docs https://docs.gitlab.com/ee/api/ 
   * Cordite Issues https://gitlab.com/api/v4/projects/4177507/issues
   * Cordite Merge Requests https://gitlab.com/api/v4/projects/4177507/merge_requests
   * Commits - https://docs.gitlab.com/ee/api/commits.html 
   * Events - https://docs.gitlab.com/ee/api/events.html 
   * Award Emoji - https://docs.gitlab.com/ee/api/award_emoji.html 
   * To Do - https://docs.gitlab.com/ee/api/todos.html 

## Ideas
   * Top 10 contributors based on time tracking or commits proposed for grants
   * Merge Requests paid a bounty
   * Higher voted the issues the higher the bounty
   * Simple propose budget for scope & community voting scheme
   * Emoji Awards have monetary value
   * Simple pledge to pay for an issue
   * InfoSec issues raised privately and approved by maintainers

## Examples
   * https://medium.com/gitcoin/tutorial-how-to-price-work-on-gitcoin-49bafcdd201e 
   * https://gitcoin.co/results
   * https://gitcoin.co/help 

