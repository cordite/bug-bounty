<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
### Public P2P address
You can use https://serveo.net/ to provide a public P2P address

```bash
ssh -o ServerAliveInterval=15 -R 10002:localhost:10002 serveo.net  
```
This process needs to run while your server is up.  It has been added as a service to docker-compose.

Note that if we're all behind the same firewall, it's likely that we won't all be able to use the same port, so pick a 
random port and use that as your ```CORDITE_P2P_PORT``` too.
  
I think we'll keep a list of ports being used in the [wiki](https://gitlab.com/cordite/bug-bounty-template/wikis/home).
  
For those without ssh you can use these untested alternatives: [ngrok](https://ngrok.com/), [pagekite](https://pagekite.net/) and [portmap](https://portmap.io/)  
