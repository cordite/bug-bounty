#!/usr/bin/env bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#


NODE_ID=${1}
NMS_HOST=${2:-nms-bb.cordite.foundation}

if [[ -z ${NODE_ID} ]]; then
  echo "you must specify which node id you want to remove"
  exit 1
fi

if [[ -z ${NMS_AUTH_PASSWORD} ]]; then
  echo "you must export NMS_AUTH_PASSWORD=<password>"
  exit 1
fi

echo "deleting node ${NODE_ID} from ${NMS_HOST}"
NMS_JWT=$(curl -X POST "https://${NMS_HOST}//admin/api/login" -H "accept: text/plain" -H "Content-Type: application/json" -d "{ \"user\": \"admin\", \"password\": \"${NMS_AUTH_PASSWORD}\"}")

curl -X DELETE "https://${NMS_HOST}/admin/api/nodes/${NODE_ID}" -H "accept: application/json" -H "Authorization: Bearer ${NMS_JWT}"
