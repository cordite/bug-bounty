/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite

import javafx.scene.paint.Color
import javafx.scene.paint.CycleMethod
import javafx.scene.paint.LinearGradient
import javafx.scene.paint.Stop
import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
  companion object {
    val mainScreen by cssclass()
    val content by cssclass()
    val heading by cssclass()
    val bottom by cssclass()
    val forms by cssclass()
    val status by cssclass()
  }

  init {
    mainScreen {
      padding = box(10.px)
      backgroundColor += LinearGradient(0.0, 0.0, 0.0, 1.0, true, CycleMethod.NO_CYCLE, Stop(0.0, c("#028aff")), Stop(1.0, c("#003780")))
//      backgroundColor += LinearGradient(0.0, 0.0, 0.0, 1.0, true, CycleMethod.NO_CYCLE, Stop(0.0, c("#41ecbf")), Stop(1.0, c("#badbe5")))
      heading {
        fontSize = 3.em
        textFill = Color.WHITE
        fontWeight = FontWeight.BOLD
      }
      content {
        padding = box(25.px)
        button {
          fontSize = 22.px
        }
        menuButton {
          fontSize = 22.px
        }
      }
      forms {
        label {
          textFill = Color.WHITE
        }
      }
      bottom {
        fontSize = 1.em
        textFill = Color.WHITE
      }
      status {

        label {
          fontSize = 2.em
          backgroundColor += c("#003780")
        }
      }
    }
  }
}